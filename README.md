# Challenge Day 3 Minesweeper Input

Group Challenge

Python program that plays mindsweeper against another python program, choosing squares with the least chance of having a mine.