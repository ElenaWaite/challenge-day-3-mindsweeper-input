import ast
from operator import itemgetter
import itertools
 
minesweeper = open('map_array.txt', 'r')
 
grid = []
for row in minesweeper:
    #ast.literal_eval - rather than just using eval ast.literal_eval raises and exception if the input isnt a valid python datatype
    # eval evaluates row into a dictionary so it can be added to the grid 
    grid.append(ast.literal_eval(row))


numbered_squares = []
for row_index in range(len(grid[0])):
    for column_index in range(len(grid[0])):
        #if the cell isn't empty then append it into a list of numbered squares
        if grid[row_index][column_index] != '0' and grid[row_index][column_index] != ' ':
            numbered_squares.append([row_index, column_index, grid[row_index][column_index]])

# 
def probabilities(empty_space,array,probability):
    for i in empty_space:
        probability[str(i[0])+str(i[1])]+=float(array[2])/float(array[3])

# the probability that each of the surrounding squares is a mine
def surrounding_squares(numbered_squares, grid):
    empty_space = []
    probability={}
    for i in range(len(grid)):
        for j in range(len(grid)):
            probability[str(i)+str(j)]=0
    for array in numbered_squares:
        counter = 0
        row_index = array[0]
        column_index = array[1]
        empty_space=[]
 
        for i in range(row_index-1, row_index+2):
            for j in range(column_index-1, column_index+2):
                if j >= 0 and i >= 0 and j < 9 and i < 9:
                    if grid[i][j] == ' ':
                        empty_space.append([i,j])
                        counter+=1
        array.append(counter)
        probabilities(empty_space,array,probability)
 

    empty_space_numbered = []
    empty_space.sort()
    counts=[]
    
    for i in range(len(empty_space)):
        count = empty_space.count(empty_space[i])
        if empty_space[i]!=empty_space[i-1]:
            counts.append(count)
        empty_space_numbered.append([array, count])
    empty_space=list(empty_space for empty_space,_ in itertools.groupby(empty_space))
    
    for i in range(len(empty_space)):
        probability[str(empty_space[i][0])+str(empty_space[i][1])]/=counts[i]

    return numbered_squares, empty_space_numbered,probability
 
 
solution = surrounding_squares(numbered_squares,grid)

solution[1].sort(key=itemgetter(1))
a = min(x[1] for x in solution[1])
for i in range(9):
    for j in range(9):
        if solution[2][str(i)+str(j)]==0:
            solution[2][str(i)+str(j)]=10000


min_prob=min(solution[2],key=solution[2].get)

dictionary = {0:'a', 1:'b', 2:'c', 3:'d', 4:'e', 5:'f', 6:'g', 7:'h', 8:'i'}
 
print(dictionary[int(min_prob[1])],int(min_prob[0])+1, 'these are empties! PICK any of these!!')
 
print()